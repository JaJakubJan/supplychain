package test.pl.jwrzdk.main.internal;

import org.junit.jupiter.api.Test;
import pl.jwrzdk.main.internal.SupplyChainParameters;;
import static org.junit.jupiter.api.Assertions.*;
/**
 * SupplyChainParameters Tester.
 *
 * @author jwrzdk
 * @since <pre>kwi 9, 2020</pre>
 * @version 1.0
 */
class SupplyChainParametersTest {

    @Test
    void create() {
        SupplyChainParameters param = SupplyChainParameters.create(2,
                3,4,1);
        assertNotNull(param);
        assertEquals(2, param.NUM_OF_DELIVERS);
        assertEquals(3, param.NUM_OF_FACTORIES);
        assertEquals(4, param.NUM_OF_MAGAZINES);
        assertEquals(1, param.NUM_OF_SHOPS);
    }

    @Test
    void createFromInvalidValues(){
        assertThrows(IllegalArgumentException.class,
                ()-> SupplyChainParameters.create(-2,
                        3,4,1));
        assertThrows(IllegalArgumentException.class,
                ()-> SupplyChainParameters.create(2,
                        -3,4,1));
        assertThrows(IllegalArgumentException.class,
                ()-> SupplyChainParameters.create(2,
                        3,-4,1));
        assertThrows(IllegalArgumentException.class,
                ()-> SupplyChainParameters.create(2,
                        3,4,-1));
        assertThrows(IllegalArgumentException.class,
                ()-> SupplyChainParameters.create(-2,
                        -3,-4,-1));
    }

    @Test
    void equalsAndHashCode(){
        SupplyChainParameters param = SupplyChainParameters.create(2,
                3,4,1);
        SupplyChainParameters param2 = SupplyChainParameters.create(2,
                3,4,1);
        assertEquals(param, param2);
    }
} 
