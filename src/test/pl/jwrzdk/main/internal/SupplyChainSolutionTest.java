package test.pl.jwrzdk.main.internal; 

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import pl.jwrzdk.main.external.ProblemAndSolutionFactory;
import pl.jwrzdk.main.external.ProblemParameters;
import pl.jwrzdk.main.external.Solution;
import pl.jwrzdk.main.internal.SupplyChainFactory;
import pl.jwrzdk.main.internal.SupplyChainParameters;
import static org.junit.jupiter.api.Assertions.*;
/**
 * RandomSupplyChainSolution Tester.
 *
 * @author jwrzdk
 * @since <pre>kwi 9, 2020</pre>
 * @version 1.0
 */
class SupplyChainSolutionTest {
    ProblemAndSolutionFactory factory;
    ProblemParameters param;

    @BeforeEach
    void before(){
        factory = SupplyChainFactory.create();
        param = SupplyChainParameters.create(3,
                4, 5, 6);
    }

    @Test
    void createRandomSolution(){
        Solution solution = factory.createRandomSolutionFrom(param);
        assertNotNull(solution);
        assertEquals(param, solution.getParameters());
    }

} 
