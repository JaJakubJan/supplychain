package test.pl.jwrzdk.main.external;

import org.junit.jupiter.api.Test;
import pl.jwrzdk.main.external.Matrix;
import static org.junit.jupiter.api.Assertions.*;
/**
 * Matrix Class Tester.
 *
 * @author jwrzdk
 * @since <pre>kwi 9, 2020</pre>
 * @version 1.0
 */
class MatrixTest {

    @Test
    void create() {
        Matrix matrix = Matrix.createFillByZeroes(3, 4);
        assertNotNull(matrix);
        assertEquals(3, matrix.ROWS);
        assertEquals(4, matrix.COLUMNS);
    }

    @Test()
    void createFromNegativeDimension(){
        assertThrows(IllegalArgumentException.class,
                ()-> Matrix.createFillByZeroes(-4, 3));
        assertThrows(IllegalArgumentException.class,
                ()-> Matrix.createFillByZeroes(10, -8));
    }

    @Test()
    void createFromZeroDimension(){
        assertThrows(IllegalArgumentException.class,
                ()-> Matrix.createFillByZeroes(0, 3));
        assertThrows(IllegalArgumentException.class,
                ()-> Matrix.createFillByZeroes(10, 0));
    }

    @Test
    void toDoubleArray() {
        double[][] matrix = Matrix.createFillByZeroes(6, 3).toDoubleArray();
        for (double[] row:  matrix) {
            assertArrayEquals(new double[]{0, 0, 0}, row);
        }
        assertEquals(6, matrix.length);
    }

    @Test
    void setRandomValidRanges() {
        Matrix.setRandomRanges(10, 100);
        assertEquals(10, Matrix.getRandMin());
        assertEquals(100, Matrix.getRandMax());
        Matrix.resetRandomRanges();
    }

    @Test
    void setRandomInvalidRanges() {
        assertThrows(IllegalArgumentException.class,
                ()-> Matrix.setRandomRanges(100, 10));
    }

    @Test
    void createFillByRandom() {
        Matrix.setRandomRanges(10, 100);
        Matrix matrix = Matrix.createFillByRandom(1000, 100);
        for (double[] row:  matrix.toDoubleArray())
            for (double element:  row) {
                assertFalse(element >= Matrix.getRandMax());
                assertFalse(element < Matrix.getRandMin());
            }
        Matrix.resetRandomRanges();
    }

    @Test
    void resetRandomRanges() {
        Matrix.setRandomRanges(-1000, 10000);
        Matrix.resetRandomRanges();
        assertEquals(Matrix.BASE_RAND_MIN, Matrix.getRandMin());
        assertEquals(Matrix.BASE_RAND_MAX, Matrix.getRandMax());
    }

    @Test
    void getValidHadamardProduct(){
        Matrix first = Matrix.createFrom(new double[][]{
                {1, 2, 3},
                {4, 5, 6},
                {7, 8, 9}});
        Matrix second = Matrix.createFrom(new double[][]{
                {9, 8, 7},
                {6, 5, 4},
                {3, 2, 1}});
        Matrix result = Matrix.getHadamardProduct(first, second);
        assertArrayEquals( new double[]{9, 16, 21}, result.getData()[0]);
        assertArrayEquals( new double[]{24, 25, 24}, result.getData()[1]);
        assertArrayEquals( new double[]{21, 16, 9}, result.getData()[2]);
    }

    @Test
    void getInvalidHadamardProduct() {
        Matrix first = Matrix.createFrom(new double[][]{
                {1, 2, 3},
                {4, 5, 6},
                {7, 8, 9}});
        Matrix second = Matrix.createFrom(new double[][]{
                {9, 8, 7},
                {6, 5, 4},});
        assertThrows(IllegalArgumentException.class,
                ()-> Matrix.getHadamardProduct(first, second));
    }

    @Test
    void getSum(){
        Matrix testMatrix = Matrix.createFrom(new double[][]{
                {1, 2, 3},
                {4, 5, 6},
                {7, 8, 9}});
        assertEquals(45, testMatrix.sum());
    }

    @Test
    void createFrom() {
        double[][] tempDoubleArray = new double[][]{{10, 100, 20},{54, 23, 12},{32, 43, 23}};
        double[][] matrix = Matrix.createFrom(tempDoubleArray).toDoubleArray();
        for (int i = 0; i < matrix.length; i++) {
            assertArrayEquals(tempDoubleArray[i], matrix[i]);
        }
    }

    @Test
    void createFromValidMultiplyArray(){
        double[] row = new double[]{1, 2, 3};
        Matrix testMatrix = Matrix.createFromMultiplyArray(row, 3);
        assertEquals( 3, testMatrix.ROWS);
        assertArrayEquals( row, testMatrix.getData()[0]);
        assertArrayEquals( row, testMatrix.getData()[1]);
        assertArrayEquals( row, testMatrix.getData()[2]);
    }

    @Test
    void setNewValue() {
        Matrix matrix = Matrix.createFillByZeroes(3, 4);
        matrix.setNewValue(1,2, 5);
        assertEquals(5, matrix.toDoubleArray()[1][2]);
        assertEquals(0, matrix.toDoubleArray()[1][3]);
        assertEquals(0, matrix.toDoubleArray()[0][2]);
    }

    @Test
    void toString1() {
        String matrixString = Matrix.createFillByZeroes(2, 3).toString();
        assertTrue(matrixString.compareTo(("   0,0000    0,0000    0,0000 " +
                "\n   0,0000    0,0000    0,0000 \n")) == 0);
    }
}