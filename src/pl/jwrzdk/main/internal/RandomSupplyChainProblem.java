package pl.jwrzdk.main.internal;

import pl.jwrzdk.main.external.Matrix;

final class RandomSupplyChainProblem extends SupplyChainProblem{
    static final double COSTS_MIN = 10;
    static final double COSTS_MAX = 1000;
    static final double ANS_MINS_MIN = 0;
    static final double ANS_MINS_MAX = 100;
    static final double ANS_MAXS_MIN = 100;
    static final double ANS_MAXS_MAX = 1000;
    static final double ENT_COST_MIN = 100;
    static final double ENT_COST_MAX = 1000;
    static final double PROFIT_MIN = 100;
    static final double PROFIT_MAX = 1000;
    static final double SIZE_MIN = 10;
    static final double SIZE_MAX = 1000;

    RandomSupplyChainProblem(final SupplyChainParameters param){
        super(param);
        Matrix.setRandomRanges(COSTS_MIN, COSTS_MAX);
        delFacCosts = Matrix.createFillByRandom(param.NUM_OF_DELIVERS, param.NUM_OF_FACTORIES);
        facMagCosts = Matrix.createFillByRandom(param.NUM_OF_FACTORIES, param.NUM_OF_MAGAZINES);
        magShpCosts = Matrix.createFillByRandom(param.NUM_OF_MAGAZINES, param.NUM_OF_SHOPS);
        Matrix.setRandomRanges(ANS_MINS_MIN, ANS_MINS_MAX);
        delFacAnsMins = Matrix.createFillByRandom(param.NUM_OF_DELIVERS, param.NUM_OF_FACTORIES);
        facMagAnsMins = Matrix.createFillByRandom(param.NUM_OF_FACTORIES, param.NUM_OF_MAGAZINES);
        magShpAnsMins = Matrix.createFillByRandom(param.NUM_OF_MAGAZINES, param.NUM_OF_SHOPS);
        Matrix.setRandomRanges(ANS_MAXS_MIN, ANS_MAXS_MAX);
        delFacAnsMaxs = Matrix.createFillByRandom(param.NUM_OF_DELIVERS, param.NUM_OF_FACTORIES);
        facMagAnsMaxs = Matrix.createFillByRandom(param.NUM_OF_FACTORIES, param.NUM_OF_MAGAZINES);
        magShpAnsMaxs = Matrix.createFillByRandom(param.NUM_OF_MAGAZINES, param.NUM_OF_SHOPS);
        Matrix.setRandomRanges(ENT_COST_MIN, ENT_COST_MAX);
        delEntCosts = Matrix.createFillByRandom(1, param.NUM_OF_DELIVERS);
        facEntCosts = Matrix.createFillByRandom(1, param.NUM_OF_FACTORIES);
        magEntCosts = Matrix.createFillByRandom(1, param.NUM_OF_MAGAZINES);
        Matrix.setRandomRanges(PROFIT_MIN, PROFIT_MAX);
        shpProfits = Matrix.createFillByRandom( 1, param.NUM_OF_SHOPS);
        Matrix.setRandomRanges(SIZE_MIN, SIZE_MAX);
        delSizes = Matrix.createFillByRandom( 1, param.NUM_OF_DELIVERS);
        facSizes = Matrix.createFillByRandom( 1, param.NUM_OF_FACTORIES);
        magSizes = Matrix.createFillByRandom( 1, param.NUM_OF_MAGAZINES);
        shpSizes = Matrix.createFillByRandom( 1, param.NUM_OF_SHOPS);
        Matrix.resetRandomRanges();
    }
}
