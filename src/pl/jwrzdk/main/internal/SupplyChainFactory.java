package pl.jwrzdk.main.internal;

import pl.jwrzdk.main.external.Problem;
import pl.jwrzdk.main.external.ProblemAndSolutionFactory;
import pl.jwrzdk.main.external.ProblemParameters;
import pl.jwrzdk.main.external.Solution;

public class SupplyChainFactory implements ProblemAndSolutionFactory {

    public static SupplyChainFactory create(){
        return new SupplyChainFactory();
    }
    @Override
    public Problem createProblemFrom(String fileName) {
        return new ReadedSupplyChainProblem(fileName);
    }

    @Override
    public Problem createRandomProblemFrom(ProblemParameters parameters) {
        if(!(parameters instanceof  SupplyChainParameters))
            throw new IllegalArgumentException("Wrong parameters class!");
        return new RandomSupplyChainProblem((SupplyChainParameters) parameters);
    }

    @Override
    public Solution createSolutionFrom(String fileName) {
        return new ReadedSupplyChainSolution(fileName);
    }

    @Override
    public Solution createRandomSolutionFrom(ProblemParameters parameters) {
        if(!(parameters instanceof  SupplyChainParameters))
            throw new IllegalArgumentException("Wrong parameters class!");
        return new RandomSupplyChainSolution((SupplyChainParameters) parameters);
    }

    private SupplyChainFactory(){}

}
