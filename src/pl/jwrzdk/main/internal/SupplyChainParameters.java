package pl.jwrzdk.main.internal;

import org.junit.platform.commons.PreconditionViolationException;
import org.junit.platform.commons.util.Preconditions;
import pl.jwrzdk.main.external.ProblemParameters;
import java.util.Objects;

public class SupplyChainParameters implements ProblemParameters {
    public final int NUM_OF_DELIVERS;
    public final int NUM_OF_FACTORIES;
    public final int NUM_OF_MAGAZINES;
    public final int NUM_OF_SHOPS;

    private SupplyChainParameters(final int NUM_OF_DELIVERS,
                                  final int NUM_OF_FACTORIES,
                                  final int NUM_OF_MAGAZINES,
                                  final int NUM_OF_SHOPS) {
        try{
            Preconditions.condition(NUM_OF_DELIVERS > 0, "Number of deliver must be positive!");
            Preconditions.condition(NUM_OF_FACTORIES > 0, "Number of factories must be positive!");
            Preconditions.condition(NUM_OF_MAGAZINES > 0, "Number of magazines must be positive!");
            Preconditions.condition(NUM_OF_SHOPS > 0, "Number of shops must be positive!");
        }catch(PreconditionViolationException e){
            throw new IllegalArgumentException(e.getMessage());
        }
        this.NUM_OF_DELIVERS = NUM_OF_DELIVERS;
        this.NUM_OF_FACTORIES = NUM_OF_FACTORIES;
        this.NUM_OF_MAGAZINES = NUM_OF_MAGAZINES;
        this.NUM_OF_SHOPS = NUM_OF_SHOPS;
    }

    public static SupplyChainParameters create(final int NUM_OF_DELIVERS,
                                               final int NUM_OF_FACTORIES,
                                               final int NUM_OF_MAGAZINES,
                                               final int NUM_OF_SHOPS){

        return new SupplyChainParameters(NUM_OF_DELIVERS,
                NUM_OF_FACTORIES, NUM_OF_MAGAZINES, NUM_OF_SHOPS);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SupplyChainParameters that = (SupplyChainParameters) o;
        return NUM_OF_DELIVERS == that.NUM_OF_DELIVERS &&
                NUM_OF_FACTORIES == that.NUM_OF_FACTORIES &&
                NUM_OF_MAGAZINES == that.NUM_OF_MAGAZINES &&
                NUM_OF_SHOPS == that.NUM_OF_SHOPS;
    }

    @Override
    public int hashCode() {
        return Objects.hash(NUM_OF_DELIVERS, NUM_OF_FACTORIES, NUM_OF_MAGAZINES, NUM_OF_SHOPS);
    }

    @Override
    public String toString() {
        return "SupplyChainParam{" +
                "DEL:" + NUM_OF_DELIVERS +
                ", FAC:" + NUM_OF_FACTORIES +
                ", MAG:" + NUM_OF_MAGAZINES +
                ", SHP:" + NUM_OF_SHOPS +
                '}';
    }
}
