package pl.jwrzdk.main.internal;

final class ReadedSupplyChainProblem extends SupplyChainProblem{

    ReadedSupplyChainProblem(String fileName){
        super(SupplyChainParameters.create(1,
                1,1,1));
    }
}
