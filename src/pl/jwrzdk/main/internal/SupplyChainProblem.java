package pl.jwrzdk.main.internal;

import pl.jwrzdk.main.external.Matrix;
import pl.jwrzdk.main.external.Problem;
import pl.jwrzdk.main.external.Solution;

abstract class SupplyChainProblem implements Problem {
    protected final SupplyChainParameters PARAMETERS;
    protected Matrix delFacCosts;
    protected Matrix facMagCosts;
    protected Matrix magShpCosts;
    protected Matrix delFacAnsMins;
    protected Matrix facMagAnsMins;
    protected Matrix magShpAnsMins;
    protected Matrix delFacAnsMaxs;
    protected Matrix facMagAnsMaxs;
    protected Matrix magShpAnsMaxs;
    protected Matrix delEntCosts;
    protected Matrix facEntCosts;
    protected Matrix magEntCosts;
    protected Matrix shpProfits;
    protected Matrix delSizes;
    protected Matrix facSizes;
    protected Matrix magSizes;
    protected Matrix shpSizes;

    public double getQualityFrom(final Solution solution){
        if(!(solution instanceof  SupplyChainSolution))
            throw new IllegalArgumentException("Wrong solution class!");
        SupplyChainSolution supplyChainSolution = (SupplyChainSolution) solution;
        return getSalesProfit(supplyChainSolution) - getGeneralCost(supplyChainSolution)
                - getContractCost(supplyChainSolution);
    }

    private double getGeneralCost(final SupplyChainSolution solution){
        return Matrix.getHadamardProduct(delFacCosts,  solution.DelFacAnswers).sum()
                + Matrix.getHadamardProduct(facMagCosts,  solution.FacMagAnswers).sum()
                + Matrix.getHadamardProduct(magShpCosts,  solution.MagShpAnswers).sum();
    }

    private double getSalesProfit(final SupplyChainSolution solution){
        return Matrix.getHadamardProduct(Matrix.createFromMultiplyArray(shpProfits.getData()[0],
                PARAMETERS.NUM_OF_MAGAZINES),  solution.MagShpAnswers).sum();
    }

    private double getContractCost(final SupplyChainSolution solution){
        double contractsCost = 0;
        for (int i = 0; i < PARAMETERS.NUM_OF_DELIVERS; i++)
            if(Matrix.createFromMultiplyArray(solution.DelFacAnswers.getData()[i], 1).sum() != 0)
                contractsCost += delEntCosts.getData()[0][i];
        for (int i = 0; i < PARAMETERS.NUM_OF_FACTORIES; i++)
            if(Matrix.createFromMultiplyArray(solution.FacMagAnswers.getData()[i], 1).sum() != 0)
                contractsCost += facEntCosts.getData()[0][i];
        for (int i = 0; i < PARAMETERS.NUM_OF_MAGAZINES; i++)
            if(Matrix.createFromMultiplyArray(solution.MagShpAnswers.getData()[i], 1).sum() != 0)
                contractsCost += magEntCosts.getData()[0][i];
        return contractsCost;
    }

    protected SupplyChainProblem(final SupplyChainParameters param) {
        PARAMETERS = param;
    }
}
