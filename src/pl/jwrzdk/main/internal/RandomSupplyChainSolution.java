package pl.jwrzdk.main.internal;

import pl.jwrzdk.main.external.Matrix;

public class RandomSupplyChainSolution extends SupplyChainSolution {
    static final double DEL_FAC_ANS_MIN = 0;
    static final double DEL_FAC_ANS_MAX = 1000;
    static final double FAC_MAG_ANS_MIN = 0;
    static final double FAC_MAG_ANS_MAX = 1000;
    static final double MAG_SHP_ANS_MIN = 0;
    static final double MAG_SHP_ANS_MAX = 1000;

    RandomSupplyChainSolution(final SupplyChainParameters param){
        super(param);
        Matrix.setRandomRanges(DEL_FAC_ANS_MIN, DEL_FAC_ANS_MAX);
        DelFacAnswers = Matrix.createFillByRandom(param.NUM_OF_DELIVERS, param.NUM_OF_FACTORIES);
        Matrix.setRandomRanges(FAC_MAG_ANS_MIN, FAC_MAG_ANS_MAX);
        FacMagAnswers = Matrix.createFillByRandom(param.NUM_OF_FACTORIES, param.NUM_OF_MAGAZINES);
        Matrix.setRandomRanges(MAG_SHP_ANS_MIN, MAG_SHP_ANS_MAX);
        MagShpAnswers = Matrix.createFillByRandom(param.NUM_OF_MAGAZINES, param.NUM_OF_SHOPS);
        Matrix.resetRandomRanges();
    }
}
