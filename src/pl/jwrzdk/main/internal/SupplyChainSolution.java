package pl.jwrzdk.main.internal;

import pl.jwrzdk.main.external.Matrix;
import pl.jwrzdk.main.external.ProblemParameters;
import pl.jwrzdk.main.external.Solution;

public abstract class SupplyChainSolution implements Solution {
    protected final SupplyChainParameters PARAMETERS;
    protected Matrix DelFacAnswers;
    protected Matrix FacMagAnswers;
    protected Matrix MagShpAnswers;

    protected SupplyChainSolution(final SupplyChainParameters param) {
        this.PARAMETERS = param;
    }

    @Override
    public ProblemParameters getParameters() {
        return PARAMETERS;
    }

    public Matrix getDelFacAnswers() {
        return DelFacAnswers;
    }

    public Matrix getFacMagAnswers() {
        return FacMagAnswers;
    }

    public Matrix getMagShpAnswers() {
        return MagShpAnswers;
    }
}
