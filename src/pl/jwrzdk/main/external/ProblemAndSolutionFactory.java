package pl.jwrzdk.main.external;

public interface ProblemAndSolutionFactory {
    Problem createProblemFrom(String fileName);
    Problem createRandomProblemFrom(ProblemParameters parameters);
    Solution createSolutionFrom(String fileName);
    Solution createRandomSolutionFrom(ProblemParameters parameters);
}
