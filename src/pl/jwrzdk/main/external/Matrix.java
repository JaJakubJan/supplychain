package pl.jwrzdk.main.external;

final public class Matrix {
    public final int ROWS;
    public final int COLUMNS;
    public static final double BASE_RAND_MAX = 100.0;
    public static final double BASE_RAND_MIN = 0.0;
    private static double randMax = BASE_RAND_MAX;
    private static double randMin = BASE_RAND_MIN;
    private final double[][] DATA;

    public static Matrix createFillByRandom(final int ROWS, final int COLUMNS) {
        Matrix matrix = new Matrix(ROWS, COLUMNS);
        for (int i = 0; i < ROWS; i++)
            for (int j = 0; j < COLUMNS; j++)
                matrix.DATA[i][j] = Math.random() * (randMax - randMin) + randMin;
        return matrix;
    }

    public static Matrix createFromMultiplyArray(final double[] array, final int times) {
        Matrix matrix = new Matrix(times, array.length);
        for (int i = 0 ; i < times ; i++)
                matrix.DATA[i] = array;
        return matrix;
    }

    public static void setRandomRanges(double newRandMin, double newRandMax){
        if(newRandMin >= newRandMax)
            throw new IllegalArgumentException("Wrong random min max parameters!");
        randMin = newRandMin;
        randMax = newRandMax;
    }

    public static void resetRandomRanges(){
        randMin = BASE_RAND_MIN;
        randMax = BASE_RAND_MAX;
    }

    public static Matrix createFillByZeroes(final int ROWS, final int COLUMNS) {
        return new Matrix(ROWS, COLUMNS);
    }

    public static Matrix createFrom(final double[][] DATA) {
        return new Matrix(DATA);
    }

    public static Matrix getHadamardProduct(Matrix first, Matrix second){
        if(first.COLUMNS != second.COLUMNS ||
                first.ROWS != second.ROWS)
            throw new IllegalArgumentException("Different dimensions in input matrices!");
        Matrix product = Matrix.createFrom(first.toDoubleArray());
        for (int i = 0 ; i < first.ROWS ; i++)
            for (int j = 0; j < first.COLUMNS; j++)
                product.getData()[i][j] *= second.getData()[i][j];
        return product;
    }

    public double sum(){
        double sum = 0;
        for (int i = 0 ; i < ROWS ; i++)
            for (int j = 0; j < COLUMNS; j++)
                sum += getData()[i][j];
        return sum;
    }

    private Matrix(final int ROWS, final int COLUMNS) {
        if(ROWS <= 0 || COLUMNS <= 0)
            throw new IllegalArgumentException("Wrong dimension of matrix!");
        this.ROWS = ROWS;
        this.COLUMNS = COLUMNS;
        DATA = new double[ROWS][COLUMNS];
    }

    private Matrix(double[][] DATA) {
        ROWS = DATA.length;
        COLUMNS = DATA[0].length;
        this.DATA = new double[ROWS][COLUMNS];
        for (int i = 0; i < ROWS; i++)
            System.arraycopy(DATA[i], 0, this.DATA[i], 0, COLUMNS);
    }

    public void setNewValue(int row, int column, double newValue){
        if(ROWS <= row || row < 0 ||COLUMNS <= column || column < 0)
            throw new IndexOutOfBoundsException("Wrong row and column indexes!");
        DATA[row][column] = newValue;
    }

    public double[][] toDoubleArray(){
        return DATA.clone();
    }

    public double[][] getData(){
        return DATA;
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < ROWS; i++) {
            for (int j = 0; j < COLUMNS; j++)
                stringBuilder.append(String.format("%9.4f ", DATA[i][j]));
            stringBuilder.append("\n");
        }
        return stringBuilder.toString();
    }

    public static double getRandMax() {
        return randMax;
    }

    public static double getRandMin() {
        return randMin;
    }
}
