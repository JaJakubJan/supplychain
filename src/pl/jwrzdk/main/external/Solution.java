package pl.jwrzdk.main.external;

public interface Solution {
    ProblemParameters getParameters();
}
